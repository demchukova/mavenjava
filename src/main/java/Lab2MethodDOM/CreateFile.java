package Lab2MethodDOM;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;

public class CreateFile {

    public static void main(String argv[]) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();


            Element rootElement = doc.createElement("CATALOG");
            doc.appendChild(rootElement);

            // cd element
            Element cd = doc.createElement("CD");
            rootElement.appendChild(cd);

            Element cd1 = doc.createElement("CD");
            rootElement.appendChild(cd1);

            Element cd2 = doc.createElement("CD");
            rootElement.appendChild(cd2);



            setAttributes(doc, cd,"TITLE", "Empire Burlesque" );
            setAttributes(doc, cd,"ARTIST", "BAND","NO", "Bob Dylan" );
            setAttributes(doc, cd,"COUNTRY",  "USA" );
            setAttributes(doc, cd,"PRICE","CURRENCY", "USD",  "10.5" );

            setAttributes(doc, cd1, "TITLE", "Yellow submarine");
            setAttributes(doc, cd1, "ARTIST","BAND", "YES", "The Beatles");
            setAttributes(doc, cd1, "COUNTRY", "UK");
            setAttributes(doc, cd1, "PRICE", "CURRENCY", "USD", "12.56");

            setAttributes(doc, cd2, "TITLE", "Eros");
            setAttributes(doc, cd2, "ARTIST", "BAND", "NO", "Eros Ramazzotti");
            setAttributes(doc, cd2, "COUNTRY", "UK");
            setAttributes(doc, cd2,"PRICE", "CURRENCY", "EUR", "8.50");


            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("DOMFile.xml"));
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void setAttributes(Document doc, Element cd, String titlename, String  attr, String value, String text){
        Element title = doc.createElement(titlename);
        Attr attrType = doc.createAttribute(attr);
        attrType.setValue(value);
        title.setAttributeNode(attrType);
        title.appendChild(doc.createTextNode(text));
        cd.appendChild(title);



    }
    public static void setAttributes(Document doc, Element cd, String titlename, String text){
        Element title = doc.createElement(titlename);
        title.appendChild(doc.createTextNode(text));
        cd.appendChild(title);
    }
}
