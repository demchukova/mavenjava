package Lab2MethodDOM;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;


public class QueryFile {

    public static void main(String argv[]) {

        try {
            File inputFile = new File("DOMFile.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.print("Root element: ");
            System.out.println(doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("CD");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.print("\nCurrent Element :");
                System.out.println(nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    NodeList cdTitle = eElement.getElementsByTagName("TITLE");
                    NodeList cdArtist = eElement.getElementsByTagName("ARTIST");
                    NodeList cdCountry = eElement.getElementsByTagName("COUNTRY");
                    NodeList cdPrice = eElement.getElementsByTagName("PRICE");


                    for (int count = 0; count < cdTitle.getLength(); count++) {
                        Node node1 = cdTitle.item(count);
                        Node node2 = cdArtist.item(count);
                        Node node3 = cdCountry.item(count);
                        Node node4 = cdPrice.item(count);


                        if (node1.getNodeType() == node1.ELEMENT_NODE) {
                            Element title = (Element) node1;
                            Element artist = (Element) node2;
                            Element country = (Element) node3;
                            Element price = (Element) node4;


                            System.out.print("Title: ");
                            System.out.println(title.getTextContent());

                            System.out.print("Artist: ");
                            System.out.print(artist.getTextContent());

                            System.out.print(", band : ");
                            System.out.println(artist.getAttribute("BAND"));

                            System.out.print("Country: ");
                            System.out.println(country.getTextContent());

                            System.out.print("Price: ");
                            System.out.print(price.getTextContent());

                            System.out.print(", currency : ");
                            System.out.println(price.getAttribute("CURRENCY"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
