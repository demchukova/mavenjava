package Lab2MethodDOM;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.*;

public class ParseFile {

    public static void main(String[] args) {

        try {
            File inputFile = new File("StAXFile.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            System.out.println("----------------------------");

            NodeList nList = doc.getElementsByTagName("CD");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    System.out.println("Title : "
                            + eElement
                            .getElementsByTagName("TITLE")
                            .item(0)
                            .getTextContent());
                    System.out.println("Artist : "
                            +eElement
                            .getElementsByTagName("ARTIST")
                            .item(0)
                            .getTextContent());
                    System.out.println("Country : "
                            + eElement
                            .getElementsByTagName("COUNTRY")
                            .item(0)
                            .getTextContent());
                    System.out.println("Price : "
                            + eElement
                            .getElementsByTagName("PRICE")
                            .item(0)
                            .getTextContent());

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
