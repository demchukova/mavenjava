package MavenLearning;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import java.io.*;


public class Deserialize {


    public static void  deserializeJson() throws FileNotFoundException {
        Gson gson = new Gson();
        User user = gson.fromJson(new FileReader("user.json"), User.class);
    }

    public void deserializeXML() throws IOException {

       String str = "<User><fullName>Ivan</fullName><email>icosterin@gmail.com</email></User>";
       User user = (User) xstream.fromXML(str);

        System.out.println(user.toString());

    }
}
