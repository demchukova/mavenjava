package MavenLearning;



public class App
{
    public static void main(String[] args) throws Exception {
        CreateJsonFile example = new CreateJsonFile();
        SerializeObject serializeObject = new SerializeObject();
        serializeObject.serializeToJSON();
        serializeObject.serializeToXML();
        Deserialize des = new Deserialize();
        des.deserializeJson();
        des.deserializeXML();
    }
}
