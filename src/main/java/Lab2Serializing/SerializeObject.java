package MavenLearning;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.beans.XMLEncoder;
import java.io.*;


public class SerializeObject {

  public static void serializeToJSON(){
      User user = CreateUser();

      Gson gson = new Gson();
      String json = gson.toJson(user);
      System.out.println(json);

      //2. Convert object to JSON string and save into a file directly
      try (FileWriter writer = new FileWriter("user.json")) {

          gson.toJson(user, writer);

      } catch (IOException e) {
          e.printStackTrace();
      }

  }
  public void serializeToXML() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        User user = CreateUser();
        xmlMapper.writeValue(new File("user.xml"), user);
     }





    public static User CreateUser(){
        Date date = new Date(12, 21, 1996);
        User user = new User("Ivan", "icosterin@gmail.com", date);

        List<String> skills = new ArrayList<>();
        skills.add("java");
        skills.add("python");
        skills.add("shell");

        user.setSkills(skills);
        return user;
   }
}
