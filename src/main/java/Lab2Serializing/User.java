package MavenLearning;


import java.io.Serializable;
import java.util.List;

public class User implements Serializable{
    private String fullName;
    private String email;
    private Date dateOfBirth;
    private List skills;



    public User(String fullName, String email, Date dateOfBirth) {

       setFullName(fullName);
       setEmail(email);
       setDateOfBirth(dateOfBirth);
    }


    public User(String fullName, String email) {

        setFullName(fullName);
        setEmail(email);
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFullName() {

        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setSkills(List skills) {
        this.skills = skills;
    }

    public String toString(){
        return String.format("%s, %s ", fullName, email);
    }
}
