package MavenLearning;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class CreateJsonFile
{
    public  CreateJsonFile() {


        JSONObject notification = new JSONObject();


        JSONObject targetJsonObject= new JSONObject();
        JSONArray targetJsonArray= new JSONArray();
        JSONArray targetJsonArray2= new JSONArray();

        targetJsonArray = createNode(
                "You're beautiful",
                "James Blunt",
                "45.3",
                "EUR",
                "Moldova");

        targetJsonObject.put("CD", targetJsonArray);

        targetJsonArray2= createNode(
                "Unforgiven",
                "Meatllica",
                "89.78",
                "USD",
                "Romania"
        );
        targetJsonObject.put("CD2", targetJsonArray2);



        notification.put("Catalog", targetJsonObject);
        //jsonComplex.put("Catalog", notification);
        System.out.println(notification);

        try {
            FileWriter file = new FileWriter("test.json");
            try {

                file.write(notification.toJSONString());
                file.flush();

            } finally {
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private static JSONArray createNode(String title, String artist, String price, String currency, String country)
    {
        JSONObject appsJsonObject= new JSONObject();

        JSONArray targetJsonArray= new JSONArray();

        appsJsonObject.put("Title",title);
        appsJsonObject.put("Artist",artist);
        appsJsonObject.put("Price",price);


        JSONArray platformArray = new JSONArray();
        platformArray.add(currency);

        appsJsonObject.put("currency",platformArray);
        targetJsonArray.add(appsJsonObject);
        appsJsonObject.put("Country",country);
        return targetJsonArray;

    }
}
