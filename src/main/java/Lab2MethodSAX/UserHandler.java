package Lab2MethodSAX;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {
    boolean title = false;
    boolean artist = false;
    boolean country = false;
    boolean price = false;

    @Override
    public void startElement(
            String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("TITLE")) {
            title = true;
        } else if (qName.equalsIgnoreCase("ARTIST")) {
            String band = attributes.getValue("BAND");
            System.out.print("BAND: " + band + " \n");
            artist = true;
        } else if (qName.equalsIgnoreCase("COUNTRY")) {
            country = true;
        } else if (qName.equalsIgnoreCase("PRICE")) {
            String currency = attributes.getValue("CURRENCY");
            System.out.print("CURRENCY: " + currency + " \n");
            price = true;
        }


    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {

        if (qName.equalsIgnoreCase("CD")) {
            System.out.println("End Element :" + qName);
            System.out.println("----------------");
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (title) {
            System.out.println("TITLE " + new String(ch, start, length));
            title = false;
        } else if (artist) {
            System.out.println("ARTIST " + new String(ch, start, length));
            artist = false;
        } else if (country) {
            System.out.println("COUNTRY " + new String(ch, start, length));
            country = false;
        } else if (price) {
            System.out.println("PRICE " + new String(ch, start, length));
            price = false;
        }

    }
}
