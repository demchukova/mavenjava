package Lab2MethodSAX;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.*;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class CreateFile {

    public static void main(String[] args) throws UnsupportedEncodingException {

        try {
            OutputStream outputStream = new FileOutputStream(new File("StAXFile.xml"));

            XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
                    new OutputStreamWriter(outputStream, "utf-8"));

            out.writeStartDocument();
            out.writeStartElement("CATALOG");

            out.writeStartElement("CD");
            setAttributes(out, "TITLE", "Empire Burlesque");
            setAttributes(out, "ARTIST", "BAND", "NO", "Bob Dylan");
            setAttributes(out, "COUNTRY", "USA");
            setAttributes(out, "PRICE", "CURRENCY", "USD", "10.5");
            out.writeEndElement();

            out.writeStartElement("CD");
            setAttributes(out, "TITLE", "Yellow submarine");
            setAttributes(out, "ARTIST","BAND", "YES", "The Beatles");
            setAttributes(out, "COUNTRY", "UK");
            setAttributes(out, "PRICE", "CURRENCY", "USD", "12.56");
            out.writeEndElement();

            out.writeStartElement("CD");
            setAttributes(out, "TITLE", "Eros");
            setAttributes(out, "ARTIST", "BAND", "NO", "Eros Ramazzotti");
            setAttributes(out, "COUNTRY", "UK");
            setAttributes(out,"PRICE", "CURRENCY", "EUR", "8.50");
            out.writeEndElement();


            out.writeEndElement();
            out.writeEndDocument();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setAttributes(XMLStreamWriter out, String titleName, String attrName, String attrValue, String text) throws XMLStreamException {
        out.writeStartElement(titleName);
        out.writeAttribute(attrName, attrValue);
        out.writeCharacters(text);
        out.writeEndElement();

    }

    public static void setAttributes(XMLStreamWriter out, String titleName, String text) throws XMLStreamException {
        out.writeStartElement(titleName);
        out.writeCharacters(text);
        out.writeEndElement();


    }
}
